/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50627
 Source Host           : localhost
 Source Database       : QLVB

 Target Server Type    : MySQL
 Target Server Version : 50627
 File Encoding         : utf-8

 Date: 10/23/2015 14:15:37 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` text NOT NULL,
  `email` text NOT NULL,
  `password` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
--  Table structure for `tbl_van_ban_den`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_van_ban_den`;
CREATE TABLE `tbl_van_ban_den` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ngay_den` datetime NOT NULL,
  `so_den` int(11) NOT NULL,
  `noi_gui` text COLLATE utf8_unicode_ci,
  `so_ky_hieu` text COLLATE utf8_unicode_ci,
  `ngay_thang_cong_van` datetime DEFAULT NULL,
  `nguoi_nhan` text COLLATE utf8_unicode_ci,
  `noi_dung` text COLLATE utf8_unicode_ci,
  `ky_nhan` text COLLATE utf8_unicode_ci,
  `ghi_chu` text COLLATE utf8_unicode_ci,
  `doc_upload` text COLLATE utf8_unicode_ci,
  `user_name` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=93 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
--  Table structure for `tbl_van_ban_di`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_van_ban_di`;
CREATE TABLE `tbl_van_ban_di` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ngay_thang` datetime NOT NULL,
  `ky_hieu` text COLLATE utf8_unicode_ci NOT NULL,
  `noi_dung` text COLLATE utf8_unicode_ci NOT NULL,
  `nguoi_ky` text COLLATE utf8_unicode_ci NOT NULL,
  `noi_nhan` text COLLATE utf8_unicode_ci,
  `don_vi_va_nguoi_nhan_ban_luu` text COLLATE utf8_unicode_ci,
  `so_luong` int(11) DEFAULT NULL,
  `ghi_chu` text COLLATE utf8_unicode_ci,
  `doc_upload` text COLLATE utf8_unicode_ci,
  `user_name` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

SET FOREIGN_KEY_CHECKS = 1;
