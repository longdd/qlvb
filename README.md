Phần mềm quản lý văn bản 1.0
===================


Phần mềm quản lý văn Đến hoặc Đi. Có khả năng lưu trữ files và tìm kiếm nhanh gọn.

----------

Cấu hình
-------------

Trong Soucre sẽ có một file là `config.py`, file này chứa các cấu hình cho trang web, các mô tả config sẽ được liệt kê dưới đây:


Tên     | Ý Nghĩa
-------- | ---
UPLOAD_FOLDER | `Đường dẫn đến thư mục upload file`
ALLOWED_EXTENSIONS | `Các định dạng file cho phép upload`
MYSQL_DATABASE_USER | `Username của database`
MYSQL_DATABASE_PASSWORD |`Password của database`
MYSQL_DATABASE_DB | `Tên database`
MYSQL_DATABASE_HOST | `Host của database`
ALLOWED_EXTENSIONS | `Định dạng file cho phép upload`
MAIL_SERVER | `Địa chỉ mail server`
MAIL_PORT | `Port của mail server`
MAIL_USERNAME | `Tên hòm mail`
MAIL_PASSWORD | `Pass của hòm mail`
PORT | `Cổng sẽ chạy ứng dụng`

Làm theo các bước sau để run:

 1. Cài đặt python 
 2. Làm theo các bước trong link để tạo môi trường ảo `http://docs.python-guide.org/en/latest/starting/install/win/`
 3. chạy lệnh sau để cài đặt các gói thư viện cần. `pip install -r enviroments.txt`
 4.     **Khi chạy sử dụng câu lệnh**
    
    > python app.py
