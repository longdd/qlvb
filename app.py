import datetime
from functools import wraps
from flask import (Flask, render_template, request, redirect, session,
                   send_file, url_for)
from flask.ext.mysql import MySQL
from werkzeug.utils import secure_filename
import os
import mimetypes
from flask.ext.mail import Mail, Message
from config_file import (UPLOAD_FOLDER, ALLOWED_EXTENSIONS, MYSQL_DATABASE_USER,
                         MYSQL_DATABASE_DB, MYSQL_DATABASE_HOST,
                         MYSQL_DATABASE_PASSWORD, DEBUG, MAIL_PASSWORD,
                         MAIL_PORT,
                         MAIL_SERVER, MAIL_USE_SSL, MAIL_USE_TLS, MAIL_USERNAME,
                         PORT)

mysql = MySQL()
app = Flask(__name__)

app.config["ALLOWED_EXTENSIONS"] = ALLOWED_EXTENSIONS
app.config["MYSQL_DATABASE_USER"] = MYSQL_DATABASE_USER
app.config["MYSQL_DATABASE_PASSWORD"] = MYSQL_DATABASE_PASSWORD
app.config["MYSQL_DATABASE_DB"] = MYSQL_DATABASE_DB
app.config["MYSQL_DATABASE_HOST"] = MYSQL_DATABASE_HOST
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config["DEBUG"] = DEBUG
app.secret_key = "A0Zr98j/3yX R~XHH!jmN]LWX/,?RT"

app.config.update(dict(
    MAIL_SERVER=MAIL_SERVER,
    MAIL_PORT=MAIL_PORT,
    MAIL_USE_TLS=MAIL_USE_TLS,
    MAIL_USE_SSL=MAIL_USE_SSL,
    MAIL_USERNAME=MAIL_USERNAME,
    MAIL_PASSWORD=MAIL_PASSWORD,
))

mysql.init_app(app)
mail = Mail(app)


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if session.get("username") is None:
            return redirect(url_for("login"))
        return f(*args, **kwargs)

    return decorated_function


@app.route("/login", methods=["POST", "GET"])
def login():
    if request.method == "GET":
        return render_template("login.html")

    if request.method == "POST":
        username = request.form.get('username')
        password = request.form.get('password')
        cursor = mysql.connect().cursor()
        cursor.execute(
            "SELECT * from tbl_user where username='" + username +
            "' and password='" + password + "'")
        data = cursor.fetchone()
        if data is None:
            return "Username or Password is wrong"
        else:
            session["username"] = username
            session["permission"] = data[3]
            return redirect("/")


@app.route("/send_mail_in", methods=["GET", "POST"])
@login_required
def send_mail_in():
    if request.method == "GET":
        cursor = mysql.connect().cursor()
        cursor.execute(
            "SELECT * from tbl_van_ban_den where id='" + request.args.get(
                "id") + "'")
        data = cursor.fetchone()
        cursor.close()

        return render_template("mail_form.html", attachment=data[10])

    if request.method == "POST":
        msg = Message(subject=request.form.get("subject"),
                      sender="admin@aita.gov.vn",
                      recipients=[request.form.get("to")])
        msg.html = request.form.get("content")

        if request.form.get("attachment1") != "None":
            with app.open_resource(
                            "upload/" + request.form.get("attachment1")) as fp:
                msg.attach(request.form.get("attachment1"),
                           mimetypes.guess_type(
                               request.form.get("attachment1"))[0],
                           fp.read())
        if request.form.get("attachment2") != "None":
            with app.open_resource(
                            "upload/" + request.form.get("attachment2")) as fp:
                msg.attach(request.form.get("attachment2"),
                           mimetypes.guess_type(
                               request.form.get("attachment2"))[0],
                           fp.read())

        mail.send(msg)
        return redirect("/list_in")


@app.route("/send_mail_out", methods=["GET", "POST"])
@login_required
def send_mail_out():
    if request.method == "GET":
        cursor = mysql.connect().cursor()
        cursor.execute(
            "SELECT * from tbl_van_ban_di where id='" + request.args.get(
                "id") + "'")
        data = cursor.fetchone()
        cursor.close()

        return render_template("mail_form.html", attachment=data[9])

    if request.method == "POST":
        msg = Message(subject=request.form.get("subject"),
                      sender="admin@aita.gov.vn",
                      recipients=[request.form.get("to")])
        msg.html = request.form.get("content")

        if request.form.get("attachment1") != "None":
            with app.open_resource(
                            "upload/" + request.form.get("attachment1")) as fp:
                msg.attach(request.form.get("attachment1"),
                           mimetypes.guess_type(
                               request.form.get("attachment1"))[0],
                           fp.read())

        if request.form.get("attachment2") != "None":
            with app.open_resource(
                            "upload/" + request.form.get("attachment2")) as fp:
                msg.attach(request.form.get("attachment2"),
                           mimetypes.guess_type(
                               request.form.get("attachment2"))[0],
                           fp.read())

        mail.send(msg)
        return redirect("/list_in")


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route("/new_in", methods=["POST", "GET"])
@login_required
def new_in():
    if request.method == "GET":
        return render_template("in.html")

    if request.method == "POST":
        ngay_den = request.form.get("ngay_den")
        so_den = request.form.get("so_den")
        noi_gui = request.form.get("noi_gui")
        so_ky_hieu = request.form.get("so_ky_hieu")
        ngay_thang_cong_van = request.form.get("ngay_thang_cong_van")
        nguoi_nhan = request.form.get("nguoi_nhan")
        noi_dung = request.form.get("noi_dung")
        ky_nhan = request.form.get("ky_nhan")
        ghi_chu = request.form.get("ghi_chu")

        ngay_den_format = datetime.datetime.strptime(ngay_den,
                                                     '%d-%m-%Y').strftime(
            '%Y-%m-%d')
        ngay_thang_cong_van_format = datetime.datetime.strptime(
            ngay_thang_cong_van, '%d-%m-%Y').strftime('%Y-%m-%d')

        file = request.files['fileupload1']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            savefile = secure_filename(so_den + "_" + so_ky_hieu + filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile))

        file2 = request.files['fileupload2']
        if file2 and allowed_file(file2.filename):
            filename2 = secure_filename(file2.filename)
            savefile2 = secure_filename(so_den + "_" + so_ky_hieu + filename2)
            file2.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile2))

        sum_filename = list()

        if file:
            sum_filename.append(savefile)
        if file2:
            sum_filename.append(savefile2)

        if len(sum_filename) == 2:
            sum_filename = savefile + ";" + savefile2
        elif len(sum_filename) == 1:
            sum_filename = savefile

        # cursor = mysql.connect().cursor()
        connection = mysql.get_db()
        cursor = connection.cursor()

        query = "INSERT INTO tbl_van_ban_den ( ngay_den, so_den, ky_nhan, ngay_thang_cong_van, nguoi_nhan, doc_upload, user_name, so_ky_hieu, noi_gui, noi_dung, ghi_chu) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        param = (ngay_den_format, so_den, ky_nhan, ngay_thang_cong_van_format,
                 nguoi_nhan, sum_filename, 'admin', so_ky_hieu, noi_gui,
                 noi_dung,
                 ghi_chu)

        result = cursor.execute(query, param)

        connection.commit()

        if result >= 1:
            return redirect("/list_in")
        else:
            return render_template("in.html")


@app.route("/new_out", methods=["POST", "GET"])
@login_required
def new_out():
    if request.method == "GET":
        return render_template("out.html")

    if request.method == "POST":
        ngay_thang = request.form.get("ngay_thang")
        ky_hieu = request.form.get("ky_hieu")
        noi_dung = request.form.get("noi_dung")
        nguoi_ky = request.form.get("nguoi_ky")
        noi_nhan = request.form.get("noi_nhan")
        don_vi_va_nguoi_nhan_ban_luu = request.form.get(
            "don_vi_va_nguoi_nhan_ban_luu")
        so_luong = request.form.get("so_luong")
        ghi_chu = request.form.get("ghi_chu")

        ngay_thang_format = datetime.datetime.strptime(ngay_thang,
                                                       '%d-%m-%Y').strftime(
            '%Y-%m-%d')

        file = request.files['fileupload1']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            savefile = secure_filename(ky_hieu + "_" + filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile))

        file2 = request.files['fileupload2']
        if file2 and allowed_file(file2.filename):
            filename2 = secure_filename(file2.filename)
            savefile2 = secure_filename(ky_hieu + "_" + filename)
            file2.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile2))

        sum_filename = list()

        if file:
            sum_filename.append(savefile)
        if file2:
            sum_filename.append(savefile2)

        if len(sum_filename) == 2:
            sum_filename = savefile + ";" + savefile2
        elif len(sum_filename) == 1:
            sum_filename = savefile

        # cursor = mysql.connect().cursor()
        connection = mysql.get_db()
        cursor = connection.cursor()

        query = "INSERT into tbl_van_ban_di ( noi_dung,noi_nhan,ngay_thang," \
                "don_vi_va_nguoi_nhan_ban_luu,nguoi_ky,ghi_chu,doc_upload," \
                "user_name,ky_hieu,so_luong) values (" \
                " %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        param = (
            noi_dung, noi_nhan, ngay_thang_format, don_vi_va_nguoi_nhan_ban_luu,
            nguoi_ky, ghi_chu, sum_filename, "admin", ky_hieu, so_luong)
        result = cursor.execute(query, param)

        connection.commit()

        if result >= 1:
            return redirect("/list_out")
        else:
            return render_template("out.html")


@app.route("/edit_in", methods=["POST", "GET"])
@login_required
def edit_in():
    if request.method == "GET":

        session.get("permission").split(";")
        if not "edit" in session.get("permission").split(";"):
            return render_template("401.html")

        cursor = mysql.connect().cursor()
        cursor.execute(
            "SELECT * from tbl_van_ban_den where id='" + request.args.get(
                "id") + "'")
        data = cursor.fetchone()
        cursor.close()
        return render_template("edit_in.html", data=data)

    if request.method == "POST":

        ngay_den = request.form.get("ngay_den")
        so_den = request.form.get("so_den")
        noi_gui = request.form.get("noi_gui")
        so_ky_hieu = request.form.get("so_ky_hieu")
        ngay_thang_cong_van = request.form.get("ngay_thang_cong_van")
        nguoi_nhan = request.form.get("nguoi_nhan")
        noi_dung = request.form.get("noi_dung")
        ky_nhan = request.form.get("ky_nhan")
        ghi_chu = request.form.get("ghi_chu")

        ngay_den_format = datetime.datetime.strptime(ngay_den,
                                                     '%d-%m-%Y').strftime(
            '%Y-%m-%d')
        ngay_thang_cong_van_format = datetime.datetime.strptime(
            ngay_thang_cong_van, '%d-%m-%Y').strftime('%Y-%m-%d')

        connection = mysql.get_db()
        cursor = connection.cursor()

        uploaded_filename = request.form.get("uploaded_file").split(";")

        file1 = request.files['fileupload1']
        file2 = request.files['fileupload2']

        if file1:
            if allowed_file(file1.filename):
                filename1 = secure_filename(file1.filename)
                savefile1 = secure_filename(
                    so_den + "_" + so_ky_hieu + filename1)
                file1.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile1))
                uploaded_filename[0] = savefile1
        if file2:
            if allowed_file(file2.filename):
                filename2 = secure_filename(file2.filename)
                savefile2 = secure_filename(
                    so_den + "_" + so_ky_hieu + filename2)
                file2.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile2))
                if len(uploaded_filename) == 2:
                    uploaded_filename[1] = savefile2
                else:
                    uploaded_filename.insert(1, savefile2)

        uploaded_filename_not_split = uploaded_filename[0] + ";" + \
                                      uploaded_filename[1]

        query = "UPDATE tbl_van_ban_den SET " \
                "ngay_den = %s, so_den = %s, ky_nhan =%s, " \
                "ngay_thang_cong_van =%s, nguoi_nhan =%s, " \
                "doc_upload =%s, user_name =%s, so_ky_hieu =%s, " \
                "noi_gui =%s, noi_dung =%s, ghi_chu =%s where id = %s"
        param = (
            ngay_den_format, so_den, ky_nhan,
            ngay_thang_cong_van_format,
            nguoi_nhan, uploaded_filename_not_split, 'admin', so_ky_hieu,
            noi_gui,
            noi_dung,
            ghi_chu, request.args.get("id"))

        result = cursor.execute(query, param)

        connection.commit()

        if result >= 1:
            return redirect("/list_in")
        else:
            return redirect("/list_in")

        return render_template("list_in.html")


@app.route("/edit_out", methods=["POST", "GET"])
@login_required
def edit_out():
    if request.method == "GET":
        session.get("permission").split(";")
        if not "edit" in session.get("permission").split(";"):
            return render_template("401.html")

        cursor = mysql.connect().cursor()
        cursor.execute(
            "SELECT * from tbl_van_ban_di where id='" + request.args.get(
                "id") + "'")
        data = cursor.fetchone()
        cursor.close()
        return render_template("edit_out.html", data=data)

    if request.method == "POST":
        ngay_thang = request.form.get("ngay_thang")
        ky_hieu = request.form.get("ky_hieu")
        noi_dung = request.form.get("noi_dung")
        nguoi_ky = request.form.get("nguoi_ky")
        noi_nhan = request.form.get("noi_nhan")
        don_vi_va_nguoi_nhan_ban_luu = request.form.get(
            "don_vi_va_nguoi_nhan_ban_luu")
        so_luong = request.form.get("so_luong")
        ghi_chu = request.form.get("ghi_chu")

        ngay_thang_format = datetime.datetime.strptime(ngay_thang,
                                                       '%d-%m-%Y').strftime(
            '%Y-%m-%d')

        connection = mysql.get_db()
        cursor = connection.cursor()

        uploaded_filename = request.form.get("uploaded_file").split(";")

        file1 = request.files['fileupload1']
        file2 = request.files['fileupload2']

        if file1:
            if allowed_file(file1.filename):
                filename1 = secure_filename(file1.filename)
                savefile1 = secure_filename(ky_hieu + filename1)
                file1.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile1))
                uploaded_filename[0] = savefile1
        if file2:
            if allowed_file(file2.filename):
                filename2 = secure_filename(file2.filename)
                savefile2 = secure_filename(ky_hieu + filename2)
                file2.save(os.path.join(app.config['UPLOAD_FOLDER'], savefile2))
                if len(uploaded_filename) == 2:
                    uploaded_filename[1] = savefile2
                else:
                    uploaded_filename.insert(1, savefile2)

        uploaded_filename_not_split = uploaded_filename[0] + ";" + \
                                      uploaded_filename[1]

        query = "UPDATE tbl_van_ban_di SET " \
                "noi_dung=%s, noi_nhan=%s, ngay_thang=%s, " \
                "don_vi_va_nguoi_nhan_ban_luu=%s, nguoi_ky=%s, " \
                "ghi_chu=%s, doc_upload=%s, ky_hieu=%s, " \
                "so_luong=%s WHERE id=%s "
        param = (noi_dung, noi_nhan, ngay_thang_format,
                 don_vi_va_nguoi_nhan_ban_luu, nguoi_ky, ghi_chu,
                 uploaded_filename_not_split,
                 ky_hieu, so_luong, request.args.get("id"))

        result = cursor.execute(query, param)

        connection.commit()
        if result >= 1:
            return redirect("/list_out")
        else:
            return redirect("/list_out")

        return render_template("list_out.html")


@app.route("/list_in", methods=["POST", "GET"])
@login_required
def list_in():
    if request.method == "GET":
        cursor = mysql.connect().cursor()

        cursor.execute("SELECT * from tbl_van_ban_den ORDER BY id DESC")
        data = cursor.fetchall()

        # json_data = jsonify(data=data)

        return render_template("list_in.html", data=data)

    if request.method == "POST":
        return render_template("list_in.html")


@app.route("/list_out", methods=["POST", "GET"])
def list_out():
    if request.method == "GET":
        cursor = mysql.connect().cursor()

        cursor.execute("SELECT * from tbl_van_ban_di ORDER BY id DESC")
        data = cursor.fetchall()

        # json_data = jsonify(data=data)

        return render_template("list_out.html", data=data)

    if request.method == "POST":
        return render_template("list_out.html")


@app.route('/view/<string:filename>', methods=['GET', 'POST'])
@login_required
def download1(filename):
    return send_file(app.config["UPLOAD_FOLDER"] + filename,
                     mimetype=mimetypes.guess_type(filename)[0])


@app.route("/logout", methods=["GET"])
@login_required
def logout():
    if request.method == "GET":
        session.pop("username", None)
        return redirect("/login")


@app.route("/")
@login_required
def main():
    return render_template("index.html")


@app.template_filter("format_datetime")
def format_datetime(value):
    return datetime.datetime.strptime(str(value), '%Y-%m-%d %H:%M:%S').strftime(
        '%d-%m-%Y')


@app.template_filter("get_username")
def get_username(value):
    return session.get("username")


@app.template_filter("get_permission")
def get_permission(value):
    return session.get("permission").split(";")


@app.template_filter("split_filename1")
def split_filename1(value1):
    if value1 is None:
        return None
    else:
        if len(value1.split(";")) >= 1:
            return value1.split(";")[0]


@app.template_filter("split_filename2")
def split_filename2(value):
    if value is None:
        return None
    else:
        if len(value.split(";")) == 2:
            return value.split(";")[1]


@app.route("/user_list", methods=["GET"])
@login_required
def user_list():
    if request.method == "GET":
        session.get("permission").split(";")
        if not "admin" in session.get("permission").split(";"):
            return render_template("401.html")

        cursor = mysql.connect().cursor()

        cursor.execute("SELECT * from tbl_user")
        data = cursor.fetchall()

        # json_data = jsonify(data=data)

        return render_template("user_list.html", data=data)


@app.route("/create_user", methods=["POST", "GET"])
@login_required
def create_user():
    if request.method == "GET":
        session.get("permission").split(";")
        if "admin" not in session.get("permission").split(";"):
            return render_template("401.html")

        return render_template("create_user.html")

    if request.method == "POST":
        session.get("permission").split(";")
        if "admin" not in session.get("permission").split(";"):
            return render_template("401.html")

        username = request.form.get("username")
        email = request.form.get("email")
        password = request.form.get("password")
        permission_create = request.form.get("permission_create")
        permission_edit = request.form.get("permission_edit")
        permission_search = request.form.get("permission_search")
        permission_admin = request.form.get("permission_admin")

        permission = ";"

        if permission_create:
            permission = permission_create + permission
        if permission_edit:
            permission = permission_edit + ";" + permission
        if permission_search:
            permission = permission_search + ";" + permission
        if permission_admin:
            permission = permission_admin + ";" + permission

        connection = mysql.get_db()
        cursor = connection.cursor()

        query = "INSERT INTO tbl_user (username,email,password,permission) " \
                "VALUES (%s,%s,%s,%s)"
        param = (username, email, password, permission[:-1])

        result = cursor.execute(query, param)

        connection.commit()

        if result >= 1:
            return redirect("/user_list")
        else:
            return render_template("create_user.html")


@app.errorhandler(Exception)
def page_not_found(e):
    return render_template('500.html'), 500

if __name__ == "__main__":
    app.run(port=PORT, host="0.0.0.0", threaded=True)
